import boto3
from botocore.exceptions import ClientError
import os

# Secrets Manager / SEF Vault
ACCESS_KEY = os.environ['AWS_ACCESS_KEY_ID']
SECRET_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
REGION = os.environ['AWS_REGION']
AWS_ELASTIC_IP_ID = os.environ['AWS_ELASTIC_IP_ID']
AWS_EC2_INSTANCE_V1 = os.environ['AWS_EC2_INSTANCE_V1']
AWS_EC2_INSTANCE_V2 = os.environ['AWS_EC2_INSTANCE_V2']

ec2 = boto3.client('ec2', 
   aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY, region_name=REGION)


# ID od nove masine?
# daj mi instancu gde pise Name=V2

def vezi_masinu_za_eip(instance_id):
    try:
        response = ec2.associate_address(AllocationId=AWS_ELASTIC_IP_ID,
                                        InstanceId=instance_id)
        print(response)
    except ClientError as e:
        print(e)


# try:
#     response = ec2.release_address(AllocationId='ALLOCATION_ID')
#     print('Address released')
# except ClientError as e:
#     print(e)